package main_test

import (
	"testing"

	 "../gp-lambda-read-service"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {

	tests := []struct {
		request events.APIGatewayProxyRequest
		expect  string
		err     error
	}{
		{
			// Test that the handler responds with the correct response
			// when a valid name is provided in the HTTP body
			request: events.APIGatewayProxyRequest{HTTPMethod: "GET"},
			expect:  "{\"candidates\":[{\"geometry\":{\"location\":{\"lat\":33.686446,\"lng\":73.07625220000001},\"viewport\":{\"northeast\":{\"lat\":33.68777442989271,\"lng\":73.07732752989273},\"southwest\":{\"lat\":33.68507477010727,\"lng\":73.07462787010728}}},\"name\":\"Pakistan Museum of Natural History\"}],\"status\":\"OK\"}",
			err:     nil,
		},
		{
			// Test that the handler responds ErrNameNotProvided
			// when no name is provided in the HTTP body
			request: events.APIGatewayProxyRequest{Body: "PUT"},
			expect:  "",
			err:     main.HTTPMethodNotSupported,
		},
	}

	for _, test := range tests {
		response, err := main.HandleRequest(nil, test.request)
		assert.IsType(t, test.err, err)
		assert.Equal(t, test.expect, response.Body)
	}

}
