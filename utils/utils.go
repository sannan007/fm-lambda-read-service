package utils

import (
	"encoding/json"
	"fmt"
	"../models"
)

func ReadPlainJsonByUnMarshall(jsonStr string) models.Place {
	var place models.Place
	json.Unmarshal([]byte(jsonStr), &place)

	return place
}

func ReadPlainJsonArray(jsonStr string) []map[string]interface{} {
	var results []map[string]interface{}
	json.Unmarshal([]byte(jsonStr), &results)

	for key, result := range results {

		fmt.Println("Reading Value for Key :", key)
		//Reading each value by its key
		fmt.Println("Id :", result["id"],
			"- Name :", result["name"],
			"- Department :", result["department"],
			"- Designation :", result["designation"])
	}

	return results
}

func TestListJson() {

	type Restaurants []models.Restaurant

	var restaurants = Restaurants{
		models.Restaurant{Name: "dicks", Location: "loc", Reference: 1},
		models.Restaurant{Name: "dicks", Location: "loc", Reference: 1}}

	mList, err := json.Marshal(restaurants)
	if err != nil {
		fmt.Println("my list error:", err)
		return
	}
	fmt.Println("m List Json: ", string(mList))
}

func TestObjectInitAndMarshall() {

	name :=  "frank"
	location :=  "frank"
	reference :=  float64(564)

	place := models.Place{}
	//place := service.GetPlace()
	//restaurant := service.GetRestaurant()
	restaurant := models.Restaurant{Name: name, Location: location, Reference: reference}
	mPlace, err := json.Marshal(place)
	if err != nil {
		fmt.Println("my place error :", err)
		return
	}
	mRestaurant, err := json.Marshal(restaurant)
	if err != nil {
		fmt.Println("my restaurant error :", err)
		return
	}

	fmt.Println("Place Json: ", string(mPlace))
	fmt.Println("Restaurant Json: ", string(mRestaurant))
}