package models

// Place from Map  - represent the Place model
type Place struct {
	Candidates []Candidates `json:"candidates"`
	Status     string       `json:"status"`
}

type Candidates struct {
	Geometry Geometry `json:"geometry"`
	Name     string   `json:"name"`
}

type Geometry struct {
	Location Location `json:"location"`
	Viewport ViewPort `json:"viewport"`
}

type Location struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type ViewPort struct {
	Northeast Northeast `json:"northeast"`
	Southwest Southwest `json:"southwest"`
}

type Northeast struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Southwest struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}
