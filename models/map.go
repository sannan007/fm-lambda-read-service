package models


type Map struct {
	ID        int64
	Title     string
	Content   string
}
