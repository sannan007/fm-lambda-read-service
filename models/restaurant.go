package models

// Article represent the article model
type Restaurant struct {
	Reference float64
	Name      string
	Location  string
	Completed bool
	//Author    Author
}
