package main

import (
	"fmt"
	"errors"
	"context"
	"github.com/aws/aws-lambda-go/events"
	//"github.com/aws/aws-lambda-go/lambda"
	"../gp-lambda-read-service/service"
	//"../hello-go-serverless-webapp/models"
	"encoding/json"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	// ErrNameNotProvided is thrown when a name is not provided
	HTTPMethodNotSupported = errors.New("no name was provided in the HTTP body")
	ResponseMarshalError = errors.New("cannot marhshall the object to json")
)

func HandleRequest(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("Body size = %d. \n", len(request.Body))
	fmt.Println("Headers:")
	for key, value := range request.Headers {
		fmt.Printf("  %s: %s\n", key, value)
	}
	if request.HTTPMethod == "GET" {
		fmt.Printf("GET\n")
		var place = service.GetPlace()
		fmt.Println("My Place from map api: ", place)
		mRest, err := json.Marshal(place)
		if err != nil {
			fmt.Println("Erro in  Marshalling:", err)
			return events.APIGatewayProxyResponse{}, HTTPMethodNotSupported
		}

		fmt.Println("mRest Final Res", string(mRest))
		return events.APIGatewayProxyResponse{Body: string(mRest), StatusCode: 200}, nil
	} else if request.HTTPMethod == "POST" {
		fmt.Printf("POST METHOD\n")
		return events.APIGatewayProxyResponse{Body: "POST", StatusCode: 200}, nil
	} else {
		fmt.Printf("NEITHER\n")
		return events.APIGatewayProxyResponse{}, HTTPMethodNotSupported
	}
}

func main() {
	fmt.Printf("##### LAST LOGS #####\n")
	lambda.Start(HandleRequest)

}
