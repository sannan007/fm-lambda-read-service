package service

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"os"
	"../models"
	"../utils"
)

var (
	PlacesApiBase    = "https://maps.googleapis.com/maps/api/place/findplacefromtext"
	URL              = "https://jsonplaceholder.typicode.com/todos/1"
	TypeAutoComplete = "/autocomplete"
	TypeDetails      = "/details"
	TypeSearch       = "/search"
	OutJson          = "/json"
	ApiKey           = "AIzaSyDlbLyMCDd33tJfnq0bnZJFBR0YmuVu9JA"
	qMark            = "?"
	eMark            = "="
	aMark            = "&"
	cMark            = ","
	input            = "input"
	inputType       = "inputtype"
	field            = "fields"
)

func GetPlace() models.Place {

	var results = utils.ReadPlainJsonByUnMarshall(RestCall("Museum"))

	return results
}

func GetRestaurant() models.Restaurant {

	var results = utils.ReadPlainJsonByUnMarshall(RestCall("Museum"))
	fmt.Println(results)

	return models.Restaurant{Name: "name", Completed: true, Reference: 45}
}

func RestCall(name string) string {

	finalResponse := ""
	//formatted_address,name,rating,geometry
	//https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=Museum&inputtype=textquery&fields=a=AIzaSyDlbLyMCDd33tJfnq0bnZJFBR0YmuVu9JA
	var finalUrl = PlacesApiBase + OutJson + qMark + input + eMark + name + aMark + inputType + eMark + "textquery" + aMark + field + eMark + "geometry" + cMark + "name" + aMark + "key=" + ApiKey
	println(finalUrl)
	response, err := http.Get(finalUrl)
	if err != nil {
		fmt.Println("Error in MAP Api", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()

		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}

		finalResponse = string(contents)
		fmt.Println("Rest Final Response : ", "`" +finalResponse +"`")

	}
	return finalResponse
}
